var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , pass = require('pwd');
  
var UserSchema = new Schema({
    username: { type: String, required: true, index: { unique: true } },
    __password: {
      hash: { type: String, required: true },
      salt: { type: String, required: true }
    }
});

UserSchema.statics.signup = function(username, password, done) {
  var user = new User({username: username});
  pass.hash(password, function (err, salt, hash) {
    user.set({
      __password: {
        salt: salt,
        hash: hash
      }});
    user
      .save(function (err) {
        if (err) return done(err);
        done(null, user);
      });
  });
};

UserSchema.statics.signin = function(username, password, done) {
  this
    .findOne({ username: username }, function (err, user) {
      if (err) return done(err);

      if (user && user.__password.salt) {
        pass.hash(password, user.__password.salt, function (err, hash) {
          if (user.__password.hash == hash) {
            done(err, user);
          } else {
            return done(err);
          }
        });
      } else {
        return done(err);
      }
    });
};

var User = module.exports = mongoose.model('User', UserSchema);