var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  
var ListSchema = new Schema({
    userId: { type: String, required: true },
    name: { type: String, required: true }
});


var List = module.exports = mongoose.model('List', ListSchema);