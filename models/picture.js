var mongoose = require('mongoose')
  , Schema = mongoose.Schema;
  
var PictureSchema = new Schema({
    listId: [String],
    thumb: { type: String, required: true },
    large: { type: String, required: true }
});

var Picture = module.exports = mongoose.model('Picture', PictureSchema);