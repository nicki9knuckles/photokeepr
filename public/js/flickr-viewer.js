$(function() {
  
  var IMG_ARRAY = [];     // Array to hold images as selected, before saving
  
  $('.signIn').click(function() {
    $('#signInForm').removeClass('hide');
    $('#signUpForm').addClass('hide');
  });
  
  $('.signUp').click(function() {
    $('#signUpForm').removeClass('hide');
    $('#signInForm').addClass('hide');
  });
  
  // Search Flickr
  $('#search').submit(function(e) {
    e.preventDefault();
    $('#images').html('');
    searchFlickr($('#searchValue').val());
    $('#searchValue').val('');
    return false;
  });
  
  // Create a new list
  $('#createList').submit(function(e) {
    e.preventDefault();
    createList($('#createList .name').val());
    $('#createList .name').val('');
    $('form#search').slideDown('fast');
    $('.createList').slideUp('fast');
    return false;
  });
  
  // Select an image
  $('#images').on("click", 'li.clean', selectImages);
  
  // Delete an image
  $('#images').on("click", 'li.delete', deleteImage);
  
  // User selects a list to add current images to
  $('#addToListMenu #combine').on("click", appendToList)
  
  // User chooses new list to display
  $('#listSelector').change(function() {
    getGalleryData($('#listSelector :selected').attr('value'));
  });
  
  $('.cancel').click(function() {
    $('.createList').slideUp('fast');
    $('form#search').slideDown('fast');
  });
  
  $('button.showAddMenu').click(function() {
    $('.addToList').removeClass('hide');
    $('#greyOutPage').removeClass('hide');
    $("#addToListMenu select").html('');
    getListData($("#addToListMenu select"));
  });
  
  $('.cancelCombine, #combine').click(function() {
    $('.addToList').addClass('hide');
    $('#greyOutPage').addClass('hide');
  });
  
  getListData('#listSelector');
  
  // Append a gallery of images to another list
  function appendToList() {    
    var currentListId = $('#images').data('id')
      , targetListId = $("#addToListMenu select").val();
    
    $.ajax({
      type: 'PUT',
      url: '/pictures/append',
      data: { 'currentListId': currentListId, 'targetListId': targetListId },
      success: function(result) {
        // console.log('result', result);
      }
    });
  }
  
  // Pull in images for a selected gallery
  function getGalleryData(selectedId) {
    $.ajax({
      type: 'GET',
      url: '/lists/'+selectedId,
      data: { listId: selectedId },
      success: function(result) {
        buildGallery(result);
      }
    });
  }
  
  // Assemble html for selected gallery to display
  function buildGallery(data) {    
    var images = $.map(data.pictures, function(image) {
        return('<li class="delete"><span></span><img data-listid="'+image.listId+'" data-id="'+image._id+'" data-large="'+image.large+'" src="'+image.thumb+'" /></li>');
      });

    $('#images').data('id', data.list._id);
    $('#images').html(images);
  }
  
  // Pull in data for a list of galleries
  function getListData(selector) {
    $.get('/myLists', function(data) {
      buildList(data, selector);
    });
  }
  
  // Build a select box out of gallery names
  function buildList(data, selector) {
    $(selector).html('');
    var lists = $.map(data, function(list) {
      $(selector).append('<option value="'+list._id+'">'+list.name+'</option>');
    });
    
    // Check what item is selected in list and show gallery
    $('#listSelector option:selected').each(function () {
      getGalleryData($(this).attr('value'));
     });
     
     // Show or hide the list controls if there are items
     if(data.length) {
       $('.yourLists, .addToListControls').removeClass('hide');
     } else {
       $('.yourLists, .addToListControls').addClass('hide');
     }
  }
  
  // Create a new list of images and save to db
  function createList(name) {  
    $.post("/saveList", {'name': name, 'imgs': IMG_ARRAY}, function(data) {
      getListData('#listSelector');
    });

    return false;
  }  
  
  // Remove an image from a selected gallery
  function deleteImage() {
    var imgId = $(this).children('img').attr("data-id")
      , el = $(this);
    
    // do a font end delete here
    $.ajax({
      url: '/pictures/'+imgId,
      type: 'DELETE',
      success: function(result) {
        el.fadeOut('fast');
      }
    });
  }
  
  // Add images to array as you click on them
  function selectImages() {
    if($(this).hasClass('selected')) {
      
      var selectedImgId = $(this).children('img').attr("data-id")
        , selectedImg = $(this).children('img');
        
      IMG_ARRAY = IMG_ARRAY.filter(function(img) {
        return selectedImgId != img.photoId;
      });
      
      $(this).removeClass('selected');
      
      // If no imgs are selected hide the create list form, show the search bar
      if(!IMG_ARRAY.length) {
        $('form#search').slideDown('fast');
        $('.createList').slideUp('fast');
      }      
            
    } else {
      // If createList is shown, hide the search bar and hide the ad to list button
      $('form#search').slideUp('hide');
      $('.createList').slideDown('fast');
      
      
      $(this).addClass('selected');
      var large = $(this).children('img').attr("data-large")
        , thumb = $(this).children('img').attr("src")
        , photoId = $(this).children('img').attr("data-id")
        , image = {
            "large": large, 
            "thumb": thumb,
            "photoId": photoId
          };
      
      IMG_ARRAY.push(image)
    } 
  }
  
  // Query Flickr for images via text search box
  function searchFlickr(query) {
    
    $.get('/search', { query: query }, function(data) {

      var limit = 100;
      $.each(data.rsp.photos.photo, function(i, photoData) {
        if (i < limit) {
              
          var id =     photoData.id
            , title =  photoData.title
            , secret = photoData.secret
            , server = photoData.server
            , farm =   photoData.farm
            , owner =  photoData.owner
            , base =   id+'_'+secret+'_s.jpg'
            , major =  id+'_'+secret+'_z.jpg'
            , url =    'http://farm'+farm+'.static.flickr.com/'+server+'/'+major
            , img =    'http://farm'+farm+'.static.flickr.com/'+server+'/'+base
            , html =   '<img class="fimg" data-id="'+id+'" data-large="'+url+'" src="'+img+'" />'
            , url =    'http://www.flickr.com/photos/'+owner+'/'+id
            , imgLink='<li class="clean">'+html+'</li>';            
              
          $(imgLink).appendTo("#images");
              
        }
      });
      
    });
  }
});
