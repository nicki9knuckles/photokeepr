Photokeepr
==============
http://photokeepr.herokuapp.com/

## Flicker image viewer exercise

- The App is a Flicker image viewer application. It will demonstrate both my backend and UX skills with a simple CRUD functions on public image data and mongoDB. 

### Requirements:
 
1. Create an account with my username and password.

2. Login to my account, displaying my username on the page.

3. Display a search field on the page once logged in that once used to search will go out to Flicker and find images that match the string typed in the field.

4. Images returned from Flicker are displayed in a grid layout on the page. 

5. Clicking on an image selects the image, and multiple images may be selected.

6. Once one or more images are selected a "Create List" button is enabled that when clicked creates a list of the images selected. This list can be named, given a dialog entry field, with the flicker info saved to mongo db for retrieval. 

7. Once a list is created a drop down menu appears with the names of the lists the users creates, allowing the selection of any item to retrieve the list of image data from mongo to display in the grid layout by accessing Flicker.

8. Images displayed in the lists will have a "x" hover that allows the image to be removed from the list.

9. Images selected in any list can be added to other lists with an "add" button that displays a popup of the target list to which to add the image, modifying the mongo back end to sync.

10. Logout button logs the user out returning to a blank page.

#### Notes:

- The UX is written in HTML5, JavaScript, and CSS. 
- I will not use any UX framework so as to  demonstrate my mastery of these there technologies (Twitter Bootstrap, Angular, Backbone, etc). 