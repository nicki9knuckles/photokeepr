module.exports = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 8000,
  db: process.env.MONGOLAB_URI || 'localhost/flicker-viewer',
  flickr_key: process.env.FLICKR_API
}