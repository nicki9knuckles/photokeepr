global.config = require(process.env.CONFIG || __dirname + '/config');
/**
 * Module dependencies.
 */
var http = require('http')
  , mongoose = require('mongoose')
  , express = require('express')
  , app = express()
  , User = require('./models/user')
  , List = require('./models/list')
  , Picture = require('./models/picture')
  , parser = require('xml2json');

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'your secret here' }));
  app.use(require('stylus').middleware({ src: __dirname + '/public' }));
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

// Routes

function checkAuth(req, res, next) {
  if (!req.session.user_id) {
    res.send('You are not authorized to view this page');
  } else {
    next();
  }
}

app.get('/secret', checkAuth, function (req, res) {
  res.send('if you are viewing this page it means you are logged in');
  
});

app.get('/', function(req, res) {
  if(req.session._userId) {
    res.render('index', {
      user: req.session._userId,
      username : req.session.username
    });
  } else {
    res.render('index');
  }
});

app.post('/signin', function(req, res) {
  User.signin(req.body.username, req.body.password, function(err, user) {
    if(err || !user) {
      return res.render("layout", { locals: { error: "Opps! Please sign up first. Thanks." }});
    } 
    req.session._userId = user._id;
    req.session.username = req.body.username;
    res.redirect('/')
  });
});

app.post('/signup', function (req, res) {
  User.signup(req.body.username, req.body.password, function (err, user) {
    if(err) {
      return res.render("layout", { locals: { error: "Sorry! That username is already taken." }});
    } else {
      req.session._userId = user._id;
      req.session.username = req.body.username;
      res.redirect('/')
    }
  });
});

app.get('/logout', function (req, res) {
  delete req.session._userId;
  res.redirect('/');
});

app.get('/search', function(req, res) {
  var query = req.query.query
    , url = 'http://api.flickr.com/services/rest/?method=flickr.photos.search&api_key='+config.flickr_key+'&text='+query+'&jsoncallback=?'
    , response = res;
    
  var req = http.get(url, function(res) {
    console.log('got response: ' + res.statusCode);
    var photos = "";
    res.on("data", function(chunk) {
      photos += chunk;
    });
    
    res.on('end', function(){
      var options = {
        object: true
      }
      var json = parser.toJson(photos, options);
      response.send(json);
    });
    
  }).on('error', function(e) {
    console.log("Got error: " + e.message);
  });  
});

//TODO: check auth here
app.get('/myLists', function(req, res) {
  List
    .find({userId: req.session._userId}, function (err, lists) {
      if (err) return res.send(500);
      res.json(lists);
    });
});

// Get a lists and its pictures
app.get('/lists/:listId', function(req, res) {
  
  var listId = req.query.listId
    , result = {};
  
  List
    .findOne({_id: listId}, function(err, list) {
      if (err) return res.send(500);
      
      Picture
        .find({listId: list._id}, function (err, pics) {
          if (err) return res.send(500);
          
          result.list = list;
          result.pictures = pics;
          
          res.json(result);
        });
    });
});

// Add images to target list
app.put('/pictures/append', function(req, res) {
  var targetListId = req.body.targetListId
    , currentListId = req.body.currentListId;
  
  Picture
    .update({ listId: { $in: [currentListId] } }, {
      $push: {'listId' : targetListId }
    }, {
      multi: true
    }, function (err, data) {
      console.log('Updated', data);
    });
    
});

// Delete an image from a list
app.delete('/pictures/:pictureId', function(req, res, next) {
  var picture = req.picture;
  
  picture.remove(function (err) {
    if (err) return next(err);
    
    res.json(null, 204);
  });
  
});

// Save a list, and save all the images in that list
app.post('/saveList', function(req, res) {
  var newList = new List({
    userId: req.session._userId,
    name: req.body.name
  });
  
  newList.save(function (err, list) {      
      if(err){console.log('err with list save', err);}
      var length = req.body.imgs.length;
      
      for(var i = 0; i < length; i++) {
        var picture = new Picture({
          listId: list._id,
          thumb: req.body.imgs[i].thumb,
          large: req.body.imgs[i].large
        });
        picture.save(function (err, pic) {
          if(err){console.log('err with picture save', err);}
        });
      }
      res.json(list);
    });
});
app.param('listId', function (req, res, next, id) {
  List
    .findById(id, function (err, list) {
      if (err) return next(err);
      
      if (!list) return res.json(null, 404);
      
      req.list = list;
      
      next();
    });
});
app.param('pictureId', function (req, res, next, id) {
  Picture
    .findById(id, function (err, picture) {
      if (err) return next(err);
      
      if (!picture) return res.json(null, 404);
      
      req.picture = picture;
      
      next();
    });
});

http.createServer(app).listen(config.port), function() {
  console.log('Listening on %d', config.port);
}

mongoose.connect(config.db)
var myDB = mongoose.connection;

//Error handling if conncetion fails
myDB.on('error', console.error.bind(console, 'connection error:'));
//Check if successful connection is made
myDB.once('open', function callback () {
  console.log("MY DB Connected with Mongoose");
});